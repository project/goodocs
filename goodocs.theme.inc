<?php

/**
 * @file
 * Theme functions for the Google Docs module.
 */

/**
 * Builds an HTML list for document links.
 *
 * @param $data
 *   An associative array containing data for the document set.
 * @param $limit
 *   The number of results to return.
 * @param $element
 *   An optional integer to distinguish between multiple pagers on one page.
 * @param $pager
 *   The type of pager being displayed, i.e. "full", "mini", or FALSE for no
 *   pager.
 *
 * @ingroup themeable
 */
function theme_goodocs_list($data, $limit = 10, $element = 0, $pager = FALSE) {
  $output = '';

  // Builds list of docs.
  $items = array();
  foreach ($data as $doc) {
    if (isset($doc['link']['alternate'])) {
      $items[] = theme('goodocs_list_item', $doc);
    }
  }

  // Adds list to output.
  $output .= ($items) ? theme('item_list', $items) : t('No matching Google documents.');

  // Adds pager.
  if ($pager) {
    $hook = ('mini' != $pager) ? 'pager' : 'goodocs_mini_pager';
    $output .= theme($hook, NULL, $limit, $element);
  }

  // Adds logout link.
  $text = t('Log out of Google');
  $output .= theme('goodocs_logout_link', $text);

  return $output;
}

/**
 * Process variables for goodocs-list-item.tpl.php.
 *
 * The $vars array contains the following arguments:
 * - $doc
 *
 * @ingroup themable
 * @see goodocs-list-item.tpl.php
 */
function template_preprocess_goodocs_list_item(&$vars) {
  $doc = $vars['doc'];

  // Sanitizes the document "id" and "type".
  $vars['type'] = check_plain($doc['type']);
  $vars['id'] = check_plain($doc['id']);

  // Link to Google document.
  $options = array('attributes' => array('target' => '_blank'));
  $vars['link'] = l($doc['title'], $doc['link']['alternate']['href'], $options);

  // Builds link to author and published date.
  $vars['author'] = theme('goodocs_author', $doc);
  $vars['published'] = theme('goodocs_date', strtotime($doc['published']));
}

/**
 * Formats document's author.
 *
 * @param $doc
 *   The document returned from the query.
 *
 * @ingroup themeable
 */
function theme_goodocs_author($doc) {
  $account = user_load(array('mail' => $doc['email']));
  $author = ($account) ? theme('username', $account) : check_plain($doc['name']);
  return t('Created by !author', array('!author' => $author));
}

/**
 * Formats the dates displayed in the item list.
 *
 * @param $timestamp
 *   The timestamp being converted to a date.
 *
 * @ingroup themeable
 */
function theme_goodocs_date($timestamp) {
  $date = date('M tS, Y g:ia', $timestamp);
  return t('on @date', array('@date' => $date));
}

/**
 * Returns link to Google login page.
 *
 * @param $text
 *   The text to display in the link.
 *
 * @ingroup themeable
 *
 * @todo Should this be in google_auth()?
 */
function theme_goodocs_login_link($text) {
  $options = array(
    'query' => array('current_page' => $_GET['q']),
    'attributes' => array('class' => 'goodocs-login-link'),
  );
  return l($text, 'goodocs/login', $options);
}

/**
 * Returns link to Google logout page.
 *
 * @param $text
 *   The text to display in the link.
 *
 * @ingroup themeable
 *
 * @todo Should this be in google_auth()?
 */
function theme_goodocs_logout_link($text) {
  $options = array(
    'query' => array('destination' => $_GET['q']),
    'attributes' => array('class' => 'goodocs-logout-link'),
  );
  return l($text, 'goodocs/logout', $options);
}

/**
 * Formats a mini pager.
 *
 * @param $tags
 *   An array of labels for the controls in the pager.
 * @param $limit
 *   The number of query results to display per page.
 * @param $element
 *   An optional integer to distinguish between multiple pagers on one page.
 * @param $parameters
 *   An associative array of query string parameters to append to the pager
 *   links.
 * @param $quantity
 *   The number of pages in the list.
 *
 * @return
 *   An HTML string that generates the query pager.
 *
 * @ingroup themeable
 * @see theme_views_mini_pager()
 */
function theme_goodocs_mini_pager($tags = array(), $limit = 10, $element = 0, $parameters = array(), $quantity = 9) {
  global $pager_page_array, $pager_total;

  // Calculate various markers within this pager piece:
  // Middle is used to "center" pages around the current page.
  $pager_middle = ceil($quantity / 2);
  // Current is the page we are currently paged to.
  $pager_current = $pager_page_array[$element] + 1;
  // Max is the maximum page number.
  $pager_max = $pager_total[$element];
  // End of marker calculations.

  $li_previous = theme('pager_previous', (isset($tags[1]) ? $tags[1] : t('‹‹')), $limit, $element, 1, $parameters);
  if (empty($li_previous)) {
    $li_previous = "&nbsp;";
  }

  $li_next = theme('pager_next', (isset($tags[3]) ? $tags[3] : t('››')), $limit, $element, 1, $parameters);
  if (empty($li_next)) {
    $li_next = "&nbsp;";
  }

  if ($pager_total[$element] > 1) {
    $items[] = array(
      'class' => 'pager-previous',
      'data' => $li_previous,
    );
    $items[] = array(
      'class' => 'pager-current',
      'data' => t('@current of @max', array('@current' => $pager_current, '@max' => $pager_max)),
    );

    $items[] = array(
      'class' => 'pager-next',
      'data' => $li_next,
    );
    return theme('item_list', $items, NULL, 'ul', array('class' => 'pager'));
  }
}
