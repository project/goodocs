<?php

/**
 * @file goodocs-list-item.tpl.php
 * Default theme implementation for displaying a Google Doc list item.
 *
 * Available variables:
 * - $doc: An array containing the raw, unsanitized document data.
 * - $id: The unique identifier of the document.
 * - $type: The type of document, i.e. "document", "spreadsheed", etc.
 * - $link: The link to the Google document.
 * - $author: The author of the document.
 * - $published: The date the document was published.
 *
 * @see template_preprocess_goodocs_list_item()
 */
?>
<div id="goodocs-doc-<?php print $id; ?>" class="goodocs-type-<?php print $type; ?>">
  <p class="goodocs-link"><?php print $link; ?></p>
  <span class="goodocs-details">
    <?php print $author; ?>
    <?php print $published; ?>
  </span>
</div>