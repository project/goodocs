<?php

/**
 * @file
 * Page callbacks for the Google Docs module.
 */

/**
 * Forces user to authenticate against Google.
 *
 * @todo Should this be in google_auth()?
 */
function goodocs_login() {
  // Gets path user will be redirected to.
  if (isset($_GET['current_page']) && 'goodocs/login' != $_GET['current_page']) {
    $redirect = $_GET['current_page'];
  }
  else {
    $redirect = '<front>';
  }

  // Attempts a login to Google.
  $google_auth = new stdClass();
  $google_auth->scope = 'https://docs.google.com/feeds/';
  $google_auth->redirect = $redirect;
  google_auth_required($google_auth);

  // Redirects back to page.
  drupal_goto($redirect);
}

/**
 * Logout of Google.
 *
 * @todo Should this be in google_auth()?
 */
function goodocs_logout() {
  // Gets path user will be redirected to.
  if (isset($_GET['destination']) && 'goodocs/logout' != $_GET['destination']) {
    $redirect = $_GET['destination'];
  }
  else {
    $redirect = '<front>';
  }

  // Removes token, redirects back to front page.
  unset($_SESSION['google_auth_token']);
  drupal_set_message('You are logged out of Google.');
  drupal_goto($redirect);
}

/**
 * Displays a list of google docs.
 */
function goodocs_list_page() {
  $form = array();

  $data = goodocs_list();
  $form['documents'] = array(
    '#type' => 'markup',
    '#value' => theme('goodocs_list', $data),
  );

  return $form;
}
